import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

builder = Gtk.Builder()
builder.add_from_file("data/gtk/passbook_row.glade")
builder.add_from_file("data/gtk/passbook_main.glade")
builder.add_from_file("data/gtk/passbook_field.glade")

win = builder.get_object("passbook_main_window")
fb = builder.get_object("passbook_flowbox")

css = """.custom-button {
        border-radius: 0px;
        border-width: 3px;
        background-image: none;
        background-color: @list_box_bg;
}

.custom-button:hover {
        background-color: @content_view_bg;
}"""
css_provider = Gtk.CssProvider()
css_provider.load_from_data(bytes(css, 'utf-8'))

row1 = builder.get_object("passbook_row")
header = builder.get_object("passbook_header_fields")
field1 = builder.get_object("passbook_header_field")
header.pack_end(field1, expand=False, fill=False, padding=5)
title = builder.get_object("passbook_header_title")
title.set_text("Flight")
data = builder.get_object("passbook_header_contents")
data.set_text("AA0042")

ctx = row1.get_style_context()
ctx.add_class('custom-button')

fb.insert(row1, 0)

win.connect('destroy', Gtk.main_quit)
win.show_all()
Gtk.main()
