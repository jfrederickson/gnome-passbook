import json, zipfile
from abc import ABC

class Passbook(ABC):
    meta = {}

    def __init__(self, passbook_file=None, json={}):
        if passbook_file:
            self.load_pkpass(passbook_file)

    def load_pkpass(self, pkpass):
        try:
            with ZipFile(pkpass) as data:
                meta = data.read('pass.json')

                if 'generic' in meta:
                    json_key = 'generic'
                elif 'eventTicket' in meta:
                    json_key = 'eventTicket'
                elif 'boardingPass' in meta:
                    json_key = 'boardingPass'
                elif 'storeCard' in meta:
                    json_key = 'storeCard'
                elif 'coupon' in meta:
                    json_key = 'coupon'
                else:
                    raise KeyError('Provided Passbook file does not use a supported pass style')

                self.logoText = meta[json_key]['logoText']

                # Header fields; shown near the top of the Pass page
                if 'headerFields' in meta[json_key]:
                    self.headerFields = meta[json_key]['headerFields']

                # Primary fields; shown prominently on the pass page
                if 'primaryFields' in meta[json_key]:
                    self.primaryFields = meta[json_key]['primaryFields']

                # Secondary fields; shown below the primary fields
                # (and less prominently) on the pass page
                if 'secondaryFields' in meta[json_key]:
                    self.secondaryFields = meta[json_key]['secondaryFields']

        except Exception: #TODO
            pass

