# Passbook App for GNOME

This is my attempt at a Passbook app for GNOME for use on the Librem
5.

**Current Status:** Early UI design phase

![Image of main application page](data/screenshots/mainpage.png)
